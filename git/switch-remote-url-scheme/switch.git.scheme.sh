#!/bin/bash

# ref - compare
# git clone                  git@bitbucket.org:foodstuffsni/fsnz-shipment-service-common.git
# git clone https://foods-bucket@bitbucket.org/foodstuffsni/fsnz-shipment-service-common.git

prefix='https://foods-bucket@bitbucket.org/foodstuffsni';

repo_name=$(git remote -v | grep fetch | grep -o -e  'git.*git' | grep -o -e '[^/]*.git$');
echo "repo name parsed:";
echo $repo_name;

url="${prefix}/${repo_name}";
echo -e "\nwill set remote url with:";
echo $url;

git remote set-url origin $url  &&  echo -e "\nremote url set successfully";
