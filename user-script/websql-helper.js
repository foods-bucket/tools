// ==UserScript==
// @name         MyWebSQL Helper
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  utils for web sql
// @author       Lin
// @include      https://websql-*.*.fsniwaikato.kiwi/
// @grant        none
// ==/UserScript==

(function () {
    'use strict';

    // websql is already using jquery
    console.log('jquery info:', $().jquery);

    $(function () {
        console.log("document ready!");

        let url = '';
        if (location.hostname.includes('dev.dev')) {
            url = 'database-tms-dev.dev.fsniwaikato.kiwi:5432';
        } else if (location.hostname.includes('test.test')) {
            url = 'database-tms-qa.test.fsniwaikato.kiwi:5432';
        } else if (location.hostname.includes('prod.prod')) {
            url = 'database-tms-prod.prod.fsniwaikato.kiwi:5432';
        }

        $('#server option[value=""]').attr("selected", "selected");
        $('#custom-server').css({ display: "" })
        $('#custom-server input').val(url);
        $('#server-type option[value="pgsql"]').attr("selected", "selected");
    });
})();